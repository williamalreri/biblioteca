package modelo;

public class Libro extends Article {

    private String paginas;
    private String imprenta;

    public Libro() {
        this.paginas = "";
        this.imprenta = "";
    }

    public String getPaginas() {
        return paginas;
    }

    public void setPaginas(String paginas) {
        this.paginas = paginas;
    }

    public String getImprenta() {
        return imprenta;
    }

    public void setImprenta(String imprenta) {
        this.imprenta = imprenta;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "paginas='" + paginas + '\'' +
                ", imprenta='" + imprenta + '\'' +
                "} " + super.toString();
    }
}
