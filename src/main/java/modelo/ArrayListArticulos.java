package modelo;

import configurator.Configurator;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ArrayListArticulos {
    private static ArrayListArticulos miInstance;
    private static ArrayList<Article> listaArticulos = null;

    public static synchronized ArrayListArticulos getInstance() throws IOException {
        if (miInstance == null) {
            miInstance = new ArrayListArticulos();
        }
        return miInstance;
    }

    public static synchronized void reservarArticulo(String codigo) throws IOException {
        for (Article a : listaArticulos) {
            if (codigo.equals(a.getCodigo())) {
                if (a.isReservado()) {
                    System.out.println("ARTICULO RESERVADO");
                } else {
                    a.setReservado(true);
                    guardarArreglo();
                }
            }
        }
    }

    private static void guardarArreglo() throws IOException {
        FileInputStream fis = new FileInputStream(Configurator.getArticlesPath());
        Workbook wb = WorkbookFactory.create(fis);
        Sheet sh;
        Cell cell;

        Row row = null;
        String clase = "";
        for (Article a : listaArticulos) {
            clase = a.getClass().toString().substring(13);
            sh = wb.getSheet(clase);
            row = sh.createRow(1);

            int noOfRows = sh.getLastRowNum();
            for (int i = 0; i <= noOfRows; i++) {
                String comparador = sh.getRow(i).getCell(2).toString();
                if (comparador.equalsIgnoreCase(a.getCodigo())) {
                    cell = row.createCell(2);
                    cell.setCellValue("VERDADERO");
                }
            }
        }
    }

    private ArrayListArticulos() throws IOException {
        listaArticulos = new ArrayList<Article>();
        FileInputStream fis = new FileInputStream(Configurator.getArticlesPath());
        Workbook wb = WorkbookFactory.create(fis);
        Sheet sh = wb.getSheet("Pelicula");
        int noOfRows = sh.getLastRowNum();
        for (int i = 0; i <= noOfRows; i++) {
            Pelicula p = new Pelicula();
            p.setNombre((sh.getRow(i).getCell(0)).toString());
            switch (sh.getRow(i).getCell(1).toString()) {
                case "VERDADERO":
                    p.setReservado(true);
                    break;
                case "FALSO":
                    p.setReservado(false);
                    break;
            }
            p.setCodigo(sh.getRow(i).getCell(2).toString());
            p.setDuracion(sh.getRow(i).getCell(3).toString());
            p.setCalidad(sh.getRow(i).getCell(4).toString());

            System.out.println(p.toString());
            listaArticulos.add(p);
        }
        sh = wb.getSheet("Libro");
        noOfRows = sh.getLastRowNum();
        for (int i = 0; i <= noOfRows; i++) {
            Libro l = new Libro();
            l.setNombre((sh.getRow(i).getCell(0)).toString());
            switch (sh.getRow(i).getCell(1).toString()) {
                case "VERDADERO":
                    l.setReservado(true);
                    break;
                case "FALSO":
                    l.setReservado(false);
                    break;
            }
            l.setCodigo((sh.getRow(i).getCell(2)).toString());
            l.setPaginas((sh.getRow(i).getCell(3)).toString());
            l.setImprenta((sh.getRow(i).getCell(4)).toString());
            System.out.println(l.toString());
            listaArticulos.add(l);
        }
    }

    public static ArrayList<Article> getArreglo() {

        return listaArticulos;
    }
}
