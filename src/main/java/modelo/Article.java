package modelo;

public class Article {
    private String nombre;
    private boolean reservado;
    private String codigo;

    public Article() {
        this.nombre = "";
        this.reservado = false;
        this.codigo = "";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isReservado() {
        return reservado;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Article{" +
                "nombre='" + nombre + '\'' +
                ", reservado=" + reservado +
                ", codigo='" + codigo + '\'' +
                '}';
    }
}