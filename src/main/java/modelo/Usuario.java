package modelo;

public class Usuario {
    private String nombre;
    private String userid;
    private String password;

    public Usuario() {
        this.nombre = "";
        this.userid = "";
        this.password = "";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", userid='" + userid + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
