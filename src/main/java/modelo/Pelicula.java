package modelo;

public class Pelicula extends Article {

    private String duracion;
    private String calidad;

    public Pelicula() {
        super();
        this.duracion = "";
        this.calidad = "";
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    @Override
    public String toString() {
        return "Pelicula{" +
                "duracion='" + duracion + '\'' +
                ", calidad='" + calidad + '\'' +
                "} " + super.toString();
    }
}
