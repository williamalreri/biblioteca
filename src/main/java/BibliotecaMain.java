import configurator.Configurator;
import modelo.ArrayListArticulos;
import modelo.Article;
import modelo.Usuario;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BibliotecaMain {

    public static void main(String[] args) throws IOException {
        ArrayListArticulos.getInstance();
        getUserList(Configurator.getUsersPath());

        Usuario user = new Usuario();
        Scanner s = new Scanner(System.in);
        Article a = new Article();
        System.out.println("INGRESE SU NOMBRE DE USUARIO");
        String username = s.next();
        System.out.println("INGRESE SU CONTRASEÑA");
        String password = s.next();
        System.out.println(Configurator.getArticlesPath());
        boolean cookie = false;
        for (Usuario u : getUserList(Configurator.getUsersPath())) {

            if (username.equals(u.getNombre()) && password.equals(u.getPassword())) {
                user = u;
                cookie = true;
                menu();
            } else {
                cookie = false;
            }
        }

    }

    private static void menu() throws IOException {
        boolean cookie = true;
        while (cookie == true)
        {
            System.out.println("QUE DESEA HACER");
            System.out.println("1 RESERVAR UN ARTICULO");
            System.out.println("2 BUSCAR UN ARTICULO");
            System.out.println("0 SALIR");
            Scanner s = new Scanner(System.in);
            int opcion = s.nextInt();
            switch (opcion) {
                case 0:
                    cookie = false;
                    break;
                case 1:
                    reservarArticulo();
                    break;
            }
        }
    }

    private static void reservarArticulo() throws IOException {
        System.out.println("INGRESE EL CODIGO DEL ARTICULO:");
        Scanner s = new Scanner(System.in);
        String codigo = s.nextLine();
        ArrayList<Article> nombre = ArrayListArticulos.getArreglo();
        for (Article a : nombre) {
            if (codigo.equals(a.getCodigo())) {
                ArrayListArticulos.reservarArticulo(codigo);
            }
        }
    }

    private static ArrayList<Usuario> getUserList(String usersPath) throws IOException {
        ArrayList<Usuario> userList = new ArrayList<>();
        FileInputStream fis = new FileInputStream(Configurator.getUsersPath());
        Workbook wb = WorkbookFactory.create(fis);
        Sheet sh = wb.getSheet("usuarios");
        int noOfRows = sh.getLastRowNum();
        System.out.println(noOfRows);
        for (int i = 0; i <= noOfRows; i++) {
            Usuario u = new Usuario();
            u.setNombre((sh.getRow(i).getCell(0)).toString());
            u.setUserid(sh.getRow(i).getCell(1).toString().substring(0, sh.getRow(i).getCell(1).toString().indexOf('.')));
            u.setPassword(sh.getRow(i).getCell(2).toString().substring(0, sh.getRow(i).getCell(2).toString().indexOf('.')));
            System.out.println(u.toString());
            userList.add(u);
        }
        return userList;
    }


}
