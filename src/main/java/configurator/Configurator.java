package configurator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class Configurator {

	public static String getUsersPath() throws IOException {
		return buscarConfiguracion("usersPath");
	}

	public static String getConfigPath() throws IOException {
		return buscarConfiguracion("configPath");
	}

	public static String getArticlesPath() throws IOException {
		return buscarConfiguracion("articlesPath");
	}

	private static String buscarConfiguracion(String configuracion) throws IOException {
		BufferedReader reader = Files.newBufferedReader(Paths.get(".\\src\\main\\resources\\settings.txt"));
		String linea;
		while ((linea = reader.readLine()) != null) {
			if (linea.contains(configuracion)) {
				return (linea.substring(linea.indexOf("=") + 1, linea.indexOf(";")));
			}
		}
		throw new IOException("No se pudo encontrar el archivo configuracion.txt en la carpeta del módulo", new Throwable("Archivo de configuraciones no encontrado"));
	}
}